module Pages.FrontPage exposing (uiSystem, view)

import Html.Styled exposing (Html, div, img)
import Html.Styled.Attributes exposing (src, style)
import Message exposing (..)
import UI.Button exposing (..)


view : Html Msg
view =
    div
        []
        [ img
            [ src "assets/98small.png"
            , style "top" "0%"
            , style "left" "0%"
            , style "width" "100%"
            , style "height" "100%"
            , style "position" "absolute"
            ]
            []
        ]


uiSystem : Html Msg
uiSystem =
    let
        all_button =
            Button 0 0 100 100 "Im all_button" EnterGame ""
    in
    test_button all_button
