module Pages.Page exposing (..)

import Pages.Word exposing (..)
import Process.Choice exposing (Choice)
import UI.Chara exposing (Image)


type Page
    = FrontPage
    | UIPage
    | GamePage RealPage Image Int
    | MidPage


type RealPage
    = SelfPage ( Image, Word )
    | DialogPage ( List Image, Word )
    | SpacePage Word
    | ChoosePage (List Choice)
    | MultiSelf (List ( Image, Word )) --少搞一点sos
    | MultiDialog (List ( List Image, Word ))
    | MultiSpace (List Word)
