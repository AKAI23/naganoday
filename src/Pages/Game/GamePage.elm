module Pages.Game.GamePage exposing (..)

import Html.Styled exposing (Html, div, img)
import Html.Styled.Attributes exposing (src, style)
import Message exposing (Msg(..))
import Pages.Game.ChoosePage
import Pages.Game.DialogPage
import Pages.Game.SelfPage
import Pages.Game.SpacePage
import Pages.Page exposing (RealPage(..))
import UI.Button exposing (Button, test_button, trans_button)
import UI.Chara exposing (Image)


view : RealPage -> Image -> Html Msg
view real background =
    let
        content =
            case real of
                SpacePage word ->
                    Pages.Game.SpacePage.view word

                SelfPage ( chara, word ) ->
                    Pages.Game.SelfPage.view chara word

                DialogPage ( images, word ) ->
                    Pages.Game.DialogPage.view images word

                ChoosePage listChoice ->
                    Pages.Game.ChoosePage.view listChoice

                _ ->
                    div
                        []
                        []

        all_button =
            case real of
                SpacePage _ ->
                    trans_button (Button 0 0 100 100 "Im a space" NextPage "")

                SelfPage _ ->
                    trans_button (Button 0 0 100 100 "Im a selfpage" NextPage "")

                DialogPage _ ->
                    trans_button (Button 0 0 100 100 "Im a DialogPage" NextPage "")

                ChoosePage _ ->
                    div [] []

                _ ->
                    Debug.todo "Wont here"

        bgd =
            img
                [ src background.src
                , style "top" "0%"
                , style "left" "0%"
                , style "height" "100%"
                , style "width" "100%"
                , style "outline" "none"
                , style "position" "absolute"
                ]
                []
    in
    div
        []
        [ bgd
        , content
        , all_button
        ]


uiSystem : RealPage -> Html Msg
uiSystem _ =
    let
        pause_button =
            Button 2 2 5 6 "Pause" Message.Pause ""

        save_button = 
            Button 60 60 5 6 "Save" Message.Save ""
    in
        div 
            []
            [ test_button pause_button
            , test_button save_button 
            ]
    
