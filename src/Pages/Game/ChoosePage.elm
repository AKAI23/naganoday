module Pages.Game.ChoosePage exposing (..)

import Array
import Html.Styled exposing (Html, div)
import Maybe exposing (withDefault)
import Message exposing (Msg)
import Pages.Page exposing (RealPage(..))
import Process.Choice exposing (Change(..), Choice)
import Process.State exposing (State, match_state)
import String exposing (toInt)
import UI.Button exposing (Button, test_button)
import UI.Chara exposing (Image, Light(..))



--我喜欢敢助|c|2|kann+10|taka+5
--我喜欢高明|c|1|taka+10
--c  yui  taka+10


toEffect : String -> String -> String -> ( State, Change )
toEffect tp who str =
    let
        addNum =
            String.split "+" str |> List.reverse |> List.head |> withDefault "0" |> toInt |> withDefault 0

        minusNum =
            String.split "-" str |> List.reverse |> List.head |> withDefault "0" |> toInt |> withDefault 0

        ( changeNum, toWhom ) =
            if String.contains "+" str then
                ( Add addNum, String.split "+" str |> List.head |> withDefault "yui" )

            else
                ( Sub minusNum, String.split "-" str |> List.head |> withDefault "yui" )

        speState =
            match_state tp who toWhom
    in
    ( speState, changeNum )



--我喜欢敢助|
--c|
--2|
--kann+10|
--taka+5


toChoice : String -> String -> Choice
toChoice who str =
    let
        list =
            String.split "|" str

        arr =
            Array.fromList list

        ct =
            Array.get 0 arr |> withDefault "Default Choice"

        objType =
            Array.get 1 arr |> withDefault "c"

        -- objNum =
        --     Array.get 2 arr |> withDefault "1"
        oriEffect =
            List.drop 3 list

        listEffect =
            List.map (toEffect objType who) oriEffect
    in
    Choice ct listEffect


prepare : String -> Bool
prepare str =
    if str == "" then
        False

    else
        True



--4,
--8,
--yui,
--1,
--我喜欢敢助|c|2|kann+10|taka+5,
--我喜欢高明|c|1|taka+10,,,,,


construct : List String -> ( RealPage, Image )
construct list =
    let
        arr =
            Array.fromList list

        oriChoice =
            List.drop 4 list |> List.filter prepare

        who =
            Array.get 2 arr |> withDefault "yui"

        listChoice =
            List.map (toChoice who) oriChoice

        backgroundStr =
            Array.get 3 arr |> withDefault "1"

        background =
            Image ("assets/background/" ++ backgroundStr ++ ".png") Normal
    in
    ( ChoosePage listChoice, background )


render_a_button : Choice -> Int -> Html Msg
render_a_button choice tp =
    let
        mess =
            Message.MakeChoice choice

        choiceButton =
            Button 30 (toFloat tp) 40 10 choice.content mess "1"
    in
    test_button choiceButton


view : List Choice -> Html Msg
view list =
    let
        num =
            List.length list

        tp_list =
            List.range 0 (num - 1) |> List.map (\x -> x * 25 + 20)

        list_button =
            List.map2 render_a_button list tp_list
    in
    div
        []
        list_button
