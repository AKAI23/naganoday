module Pages.Game.DialogPage exposing (..)

import Array exposing (fromList, length)
import Html.Styled exposing (Html, div)
import Maybe exposing (withDefault)
import Pages.Page exposing (RealPage(..))
import Pages.Word exposing (Word)
import Public.Name exposing (get_assets_name)
import String exposing (toInt)
import UI.Chara exposing (Image, Light(..), defaultImg, view_chara_left, view_chara_right)
import UI.Textbox exposing (view_content, view_name)



--DialogPage (List Image) Word


construct : List String -> ( RealPage, Image )
construct list =
    let
        arr =
            fromList list

        speaker =
            Array.get 3 arr |> withDefault "0" |> toInt |> withDefault 0

        personList =
            List.drop 5 list

        length =
            Array.get 2 arr |> withDefault "0" |> toInt |> withDefault 0

        ( imageList, speakerName ) =
            if length == 1 then
                single_image personList

            else if length == 2 then
                dual_image personList speaker

            else
                Debug.todo "Havent support more than three people"

        content =
            Array.get 4 arr |> withDefault "default content"

        word =
            Word speakerName content

        backgroundStr =
            Array.get 9 arr |> withDefault "1"

        background =
            Image ("assets/background/" ++ backgroundStr ++ ".png") Normal
    in
    ( DialogPage ( imageList, word ), background )


single_image : List String -> ( List Image, String )
single_image list =
    let
        arr =
            fromList list

        name =
            Array.get 0 arr |> withDefault "上原由衣"

        who =
            get_assets_name name

        -- Debug.todo (Debug.toString name)
        charaStr =
            Array.get 1 arr |> withDefault "1"

        img =
            Image ("assets/" ++ who ++ "/" ++ charaStr ++ ".png") Normal
    in
    ( [ img ], name )


dual_image : List String -> Int -> ( List Image, String )
dual_image personList speaker =
    let
        arr =
            fromList personList

        name1 =
            Array.get 0 arr |> withDefault "上原由衣"

        who1 =
            get_assets_name name1

        charaStr1 =
            Array.get 1 arr |> withDefault "1"

        name2 =
            Array.get 2 arr |> withDefault "上原由衣"

        who2 =
            get_assets_name name2

        charaStr2 =
            Array.get 3 arr |> withDefault "1"

        img1 =
            Image ("assets/" ++ who1 ++ "/" ++ charaStr1 ++ ".png") Normal

        img2 =
            Image ("assets/" ++ who2 ++ "/" ++ charaStr2 ++ ".png") Normal

        ( cimg1, cimg2, spName ) =
            if speaker == 0 then
                ( img1, { img2 | light = Dark }, name1 )

            else
                ( { img1 | light = Dark }, img2, name2 )
    in
    ( [ cimg1, cimg2 ], spName )


view_charas : List Image -> List (Html msg)
view_charas list =
    if List.length list == 2 then
        [ view_chara_left (List.head list |> withDefault defaultImg)
        , view_chara_right (List.head (List.drop 1 list) |> withDefault defaultImg)
        ]

    else
        []


view : List Image -> Word -> Html msg
view list word =
    let
        charas =
            view_charas list

        name =
            word.name

        content =
            word.content
    in
    div
        []
        (charas
            ++ [ view_content content
               , view_name name
               ]
        )
