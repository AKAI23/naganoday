module Pages.Game.SelfPage exposing (..)

import Array
import Html.Styled exposing (Html, div)
import Maybe exposing (withDefault)
import Pages.Page exposing (RealPage(..))
import Pages.Word exposing (Word)
import Public.Name exposing (get_assets_name)
import UI.Chara exposing (Image, Light(..), view_chara_mid)
import UI.Textbox exposing (view_content, view_name)


construct : List String -> ( RealPage, Image )
construct list =
    let
        arr =
            Array.fromList list

        name =
            Array.get 2 arr |> withDefault "default name"

        content =
            Array.get 3 arr |> withDefault "default content"

        charaStr =
            Array.get 4 arr |> withDefault "1"

        backgroundStr =
            Array.get 5 arr |> withDefault "1"

        word =
            Word name content

        background =
            Image ("assets/background/" ++ backgroundStr ++ ".png") Normal

        who =
            get_assets_name name

        chara =
            Image ("assets/" ++ who ++ "/" ++ charaStr ++ ".png") Normal

        page =
            SelfPage ( chara, word )
    in
    ( page, background )


view : Image -> Word -> Html msg
view chara word =
    let
        name =
            word.name

        content =
            word.content
    in
    div
        []
        [ view_chara_mid chara
        , view_content content
        , view_name name
        ]
