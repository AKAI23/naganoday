module Pages.Game.SpacePage exposing (..)

import Array
import Html.Styled exposing (Html, div)
import Maybe exposing (withDefault)
import Message exposing (Msg)
import Pages.Page exposing (RealPage(..))
import Pages.Word exposing (Word)
import UI.Chara exposing (Image, Light(..))
import UI.Textbox exposing (view_content, view_name)


construct : List String -> ( RealPage, Image )
construct list =
    let
        arr =
            Array.fromList list

        name =
            Array.get 2 arr |> withDefault "default name"

        content =
            Array.get 3 arr |> withDefault "default content"

        backgroundStr =
            Array.get 4 arr |> withDefault "001"

        background =
            Image ("assets/background/" ++ backgroundStr ++ ".png") Normal

        word =
            Word name content
    in
    ( SpacePage word, background )


view : Word -> Html Msg
view word =
    let
        name =
            word.name

        content =
            word.content
    in
    div
        []
        [ view_content content
        , view_name name
        ]
