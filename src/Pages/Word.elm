module Pages.Word exposing (..)


type alias Word =
    { name : String
    , content : String
    }
