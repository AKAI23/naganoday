module Main exposing (main)

import Array exposing (fromList, get)
import Browser
import Browser.Dom exposing (getViewport)
import Browser.Events exposing (onResize)
import Data.TextBase exposing (..)
import Html.Styled exposing (Html, div, text, toUnstyled)
import Html.Styled.Attributes exposing (style)
import Message exposing (Msg(..))
import Pages.FrontPage exposing (uiSystem, view)
import Pages.Game.GamePage
import Pages.Page exposing (..)
import Pages.Word exposing (..)
import Process.Choice exposing (Change(..), Choice, addScore, subScore)
import Process.Ending exposing (Comp(..), Condition, ControlBase, initial_control_base)
import Process.State exposing (State(..), initial_state)
import Task



---- MODEL ----


type alias Model =
    { currentPage : Page
    , size : ( Float, Float )
    , currentText : TextBase
    , playState : List ( State, Int )
    , conditionList : List ControlBase
    }


init_model : Model
init_model =
    Model FrontPage ( 0, 0 ) (TextBase 0 1 []) initial_state initial_control_base


init : ( Model, Cmd Msg )
init =
    ( init_model, Cmd.batch [ Task.perform GetViewport getViewport ] )



---- UPDATE ----


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GetViewport { viewport } ->
            ( { model
                | size =
                    ( viewport.width
                    , viewport.height
                    )
              }
            , Cmd.none
            )

        Resize width height ->
            ( { model | size = ( toFloat width, toFloat height ) }
            , Cmd.none
            )

        EnterGame ->
            -- ( { model | temperNum = 1}, Cmd.none)
            let
                newText =
                    load_new_base 0

                --start from 0.csv
            in
            ( { model
                | currentText = newText
                , currentPage = getPage 0 newText.leftPages --start from 0
              }
            , Cmd.none
            )

        NextPage ->
            ( updatePage model, Cmd.none )

        Pause ->
            ( model, Cmd.none )

        Save ->
            ( model, Cmd.none )

        MakeChoice choice ->
            ( updateState choice model
                |> updateTargetBase
                |> updatePage
            , Cmd.none
            )


choiceEffectOnState : ( State, Change ) -> List ( State, Int ) -> List ( State, Int )
choiceEffectOnState effect playState =
    case effect of
        ( state, Add score ) ->
            addScore state score playState

        ( state, Sub score ) ->
            subScore state score playState


matchEachPlayState : Condition -> ( State, Int ) -> Bool
matchEachPlayState condition ( state, score ) =
    case ( condition.what, condition.compare ) of
        ( sta, Larger ) ->
            if sta == state && score >= condition.howMany then
                True

            else
                False

        ( sta, Smaller ) ->
            if sta == state && score <= condition.howMany then
                True

            else
                False

        _ ->
            False


focusOnControlBase : List ( State, Int ) -> Condition -> Bool
focusOnControlBase playState condition =
    List.any (matchEachPlayState condition) playState


getNBfromCBList : Int -> List ( State, Int ) -> ControlBase -> Bool
getNBfromCBList curr playState controlBase =
    if curr == controlBase.current then
        List.all (focusOnControlBase playState) controlBase.conditions

    else
        False


calculateNewNB : Int -> Int -> List ( State, Int ) -> List ControlBase -> Int
calculateNewNB oldNB cb playState cbList =
    let
        gotResult =
            List.filter (getNBfromCBList cb playState) cbList

        firstResult =
            List.head gotResult
    in
        case firstResult of
            Just controlB ->
                controlB.targ

            _ ->
                oldNB


updateTargetBase : Model -> Model
updateTargetBase model =
    let
        cT =
            model.currentText

        cb =
            cT.currentBase

        oldNB =
            cT.nextBase

        newNB =
            calculateNewNB oldNB cb model.playState model.conditionList

        newTB =
            { cT | nextBase = newNB }
    in
    { model | currentText = newTB }


updateState : Choice -> Model -> Model
updateState choice model =
    let
        newPlayState =
            List.foldl choiceEffectOnState model.playState choice.effect
    in
    { model | playState = newPlayState }


updatePage : Model -> Model
updatePage model =
    let
        pageLength =
            List.length model.currentText.leftPages

        ( newText, newIndex ) =
            --newText是load的base, newIndex 是List Page 的index
            case model.currentPage of
                GamePage _ _ index ->
                    if index == pageLength - 1 then
                        ( load_new_base model.currentText.nextBase, 0 )
                        --makechoice 的时候就可以更新nextbase了
                        -- Debug.todo (Debug.toString pageLength)

                    else
                        ( model.currentText, index + 1 )

                _ ->
                    ( model.currentText, 0 )

        --problem
        newPage =
            case model.currentPage of
                GamePage _ _ _ ->
                    getPage newIndex newText.leftPages

                _ ->
                    model.currentPage
    in
    { model | currentPage = newPage, currentText = newText }


getPage : Int -> List Page -> Page
getPage index list =
    let
        arr =
            fromList list

        maybePage =
            get index arr

        res =
            case maybePage of
                Just page ->
                    page

                _ ->
                    Debug.todo "got empty loca"
    in
    res



---- VIEW ----


view : Model -> Html Msg
view model =
    let
        ( w, h ) =
            model.size

        ( wid, het ) =
            if (9 / 16 * w) >= h then
                ( 16 / 9 * h, h )

            else
                ( w, 9 / 16 * w )

        ( lef, to ) =
            if (9 / 16 * w) >= h then
                ( 0.5 * (w - wid), 0 )

            else
                ( 0, 0.5 * (h - het) )

        content =
            case model.currentPage of
                FrontPage ->
                    Pages.FrontPage.view

                GamePage subpage background _ ->
                    Pages.Game.GamePage.view subpage background

                _ ->
                    div
                        []
                        [ text "hhhhh" ]

        debugInfo =
            debug_info model

        uiSystem =
            --只放界面ui，不放下一步的button
            case model.currentPage of
                GamePage subpage _ _ ->
                    Pages.Game.GamePage.uiSystem subpage

                FrontPage ->
                    Pages.FrontPage.uiSystem

                _ ->
                    Debug.todo "Nothing"
    in
    div
        [ style "width" "100%"
        , style "height" "100%"
        , style "position" "absolute"
        , style "left" "0"
        , style "top" "0"
        , style "background-color" "#000000"
        , style "overflow" "hidden"

        --, style ""
        ]
        [ div
            [ style "width" (String.fromFloat wid ++ "px")
            , style "height" (String.fromFloat het ++ "px")
            , style "position" "absolute"
            , style "left" (String.fromFloat lef ++ "px")
            , style "top" (String.fromFloat to ++ "px")
            , style "background-color" "black"

            -- , style "background-color" bkgdColor
            -- , style "opacity" (toString opa)
            ]
            [ content
            , uiSystem
            , debugInfo
            ]
        ]


debug_info : Model -> Html msg
debug_info model =
    let
        content =
            Debug.toString model
    in
    div
        []
        [ text content ]



---- PROGRAM ----


main : Program () Model Msg
main =
    Browser.element
        { view = view >> toUnstyled
        , init = \_ -> init
        , update = update
        , subscriptions = subscriptions
        }


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.batch
        [ onResize Resize
        ]
