module Public.Name exposing (..)


get_assets_name : String -> String
get_assets_name name =
    case name of
        "上原由衣" ->
            "yui"

        "大和敢助" ->
            "kann"

        _ ->
            Debug.todo "找不到对应的assets文件夹"


all_list : List ( String, String )
all_list =
    List.map2 (\y -> \x -> ( x, y )) name_list assets_list


name_list : List String
name_list =
    [ "大和敢助"
    , "上原由衣"
    , "诸伏高明"
    ]


assets_list : List String
assets_list =
    [ "kann"
    , "yui"
    , "taka"
    ]
