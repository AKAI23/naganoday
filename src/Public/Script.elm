module Public.Script exposing (allIfText, allOriginalText)


allIfText : String
allIfText =
    """0,1,1,kann2yui>15
    0,2,1,taka2yui>12"""


allOriginalText : List String
allOriginalText =
    [ csv_0
    , csv_1
    , csv_2
    , csv_3
    ]


csv_0 : String
csv_0 =
    -- should be zero here, loading information
    """0,1,,,,,,,,
    3,0,上原由衣,所有的存在都是为了被毁灭而被设计出来的,5,,,,,
    3,1,上原由衣,在这生与死的无穷螺旋中,5,,,,,
    3,2,上原由衣,我们被永远地禁锢着,5,,,,,
    3,3,上原由衣,这是诅咒吗，抑或是惩罚吗,5,,,,,
    3,4,上原由衣,交予我们这个无解难题的神,5,,,,,
    3,5,上原由衣,是否终有一天，我们会向他拉起弓箭,5,,,,,
    1,6,大和敢助,没，没事。,3,1,,,,
    2,7,2,0,走吧，去找他,大和敢助,1,上原由衣,2,1
    1,8,上原由衣,其实……,2,1,,,,
    4,9,yui,1,我喜欢敢助|c|2|kann+10|taka+5,我喜欢高明|c|1|taka+10,,,,
    1,10,大和敢助,这样啊……,3,1,,,,"""


csv_1 : String
csv_1 =
    """1,3,,,,
    1,0,上原由衣,我喜欢小敢,1,1"""


csv_2 : String
csv_2 =
    """1,3,,,,
    1,0,上原由衣,我喜欢高明,1,1"""


csv_3 : String
csv_3 =
    """1,3,,,,
    1,0,上原由衣,总之是结束了！,1,1"""
