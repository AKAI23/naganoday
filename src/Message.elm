module Message exposing (..)

import Browser.Dom exposing (Viewport)
import Pages.Page exposing (..)
import Process.Choice exposing (Choice)


type Msg
    = GetViewport Viewport
    | Resize Int Int
    | EnterGame
    | NextPage
    | Pause -- need
    | Save
    | MakeChoice Choice
