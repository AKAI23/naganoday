module Process.Ending exposing (..)

import Array
import Maybe exposing (withDefault)
import Process.State exposing (State, toState)
import Public.Script exposing (allIfText)
import String exposing (toInt)


type Comp
    = Larger
    | Smaller
    | Equal


type alias Condition =
    { what : State
    , compare : Comp
    , howMany : Int
    }


type alias ControlBase =
    { current : Int
    , targ : Int
    , conditions : List Condition
    }



--kann2yui>15


toCondition : String -> Condition
toCondition str =
    let
        list_l =
            String.split ">" str

        list_s =
            String.split "<" str

        ( whStr, com, howStr ) =
            if String.contains ">" str then
                ( List.head list_l |> withDefault "default state"
                , Larger
                , List.head (List.reverse list_l) |> withDefault "0"
                )

            else
                ( List.head list_s |> withDefault "default state"
                , Smaller
                , List.head (List.reverse list_s) |> withDefault "0"
                )

        how =
            toInt howStr |> withDefault 0

        wh =
            toState whStr
    in
    Condition wh com how



--1,3,1,taka2yui>12


convert : String -> ControlBase
convert str =
    let
        list =
            String.split "," str

        arr =
            Array.fromList list

        curr =
            Array.get 0 arr |> withDefault "0" |> toInt |> withDefault 0

        tar =
            Array.get 1 arr |> withDefault "0" |> toInt |> withDefault 0

        length =
            Array.get 2 arr |> withDefault "0" |> toInt |> withDefault 0

        conds =
            List.drop 3 list

        condList =
            List.map toCondition conds |> List.take length

        res =
            ControlBase curr tar condList
    in
    res



-- Debug.todo (Debug.toString list)


initial_control_base : List ControlBase
initial_control_base =
    let
        oriList =
            String.words allIfText
    in
    List.map convert oriList
