module Process.State exposing (..)


type State
    = Yui2police
    | Kann2die
    | Kann2yui
    | Taka2yui
    | Taka2kann
    | Kann2taka


initial_state : List ( State, Int )
initial_state =
    let
        initial_prefer =
            [ ( Kann2yui, 10 )
            , ( Taka2yui, 5 )
            , ( Taka2kann, 10 )
            , ( Kann2taka, 8 )
            ]

        -- Prefers 10 5 10 8
        initial_possibility =
            [ ( Yui2police, 5 )
            , ( Kann2die, 0 )
            ]

        -- Possibility 5 0
    in
    initial_prefer ++ initial_possibility


match_state : String -> String -> String -> State
match_state tp who whom =
    case ( tp, who, whom ) of
        ( "c", "yui", "taka" ) ->
            Taka2yui

        ( "c", "yui", "kann" ) ->
            Kann2yui

        ( _, _, _ ) ->
            Debug.todo ("Missing matching state" ++ tp ++ who ++ whom)


toState : String -> State
toState str =
    case str of
        "yui2police" ->
            Yui2police

        "kann2die" ->
            Kann2die

        "kann2yui" ->
            Kann2yui

        "taka2yui" ->
            Taka2yui

        "taka2kann" ->
            Taka2kann

        "kann2taka" ->
            Kann2taka

        _ ->
            Debug.todo "can not find such state"
