module Process.Choice exposing (..)

import Process.State exposing (State)


type Change
    = Add Int
    | Sub Int


type alias Choice =
    { content : String
    , effect : List ( State, Change )
    }


addOne : State -> Int -> ( State, Int ) -> ( State, Int )
addOne state score ( targetState, oldScore ) =
    if state == targetState then
        ( targetState, oldScore + score )

    else
        ( targetState, oldScore )


subOne : State -> Int -> ( State, Int ) -> ( State, Int )
subOne state score ( targetState, oldScore ) =
    if state == targetState then
        ( targetState, oldScore - score )

    else
        ( targetState, oldScore )


addScore : State -> Int -> List ( State, Int ) -> List ( State, Int )
addScore prefer score list =
    List.map (addOne prefer score) list


subScore : State -> Int -> List ( State, Int ) -> List ( State, Int )
subScore prefer score list =
    List.map (subOne prefer score) list
