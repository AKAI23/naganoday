module UI.Textbox exposing (..)

import Debug exposing (toString)
import Html.Styled exposing (Html, div)
import Html.Styled.Attributes exposing (style)
import Html.Styled exposing (Attribute)

type Tex
    = Contents
    | NameBox


type alias Textbox =
    { content : String
    , fontSize : Int
    , start_loca : ( Int, Int )
    , size : ( Int, Int )
    , tp : Tex
    }


view_content : String -> Html msg
view_content str =
    real_content str |> test_textbox


view_name : String -> Html msg
view_name str =
    real_name str |> test_textbox


real_content : String -> Textbox
real_content str =
    Textbox str 25 ( 10, 65 ) ( 35, 80 ) Contents


real_name : String -> Textbox
real_name str =
    Textbox str 30 ( 15, 60 ) ( 7, 15 ) NameBox


content_special : List (Attribute msg)
content_special =
    [ style "padding" "2%"
    ]



test_textbox : Textbox -> Html msg
test_textbox textBox =
    let
        content =
            textBox.content

        fontSize =
            textBox.fontSize

        ( hei, wid ) =
            textBox.size

        ( lef, tp ) =
            textBox.start_loca
        extra = 
            case textBox.tp of
                Contents ->
                    content_special
                NameBox ->
                    [ style "text-align" "center"]
    in
    div
        ([ style "top" (toString tp ++ "%")
        , style "left" (toString lef ++ "%")
        , style "height" (toString hei ++ "%")
        , style "width" (toString wid ++ "%")
        , style "cursor" "pointer"
        , style "outline" "none"
        , style "padding" "0"
        , style "position" "absolute"
        , style "font-size" (toString fontSize ++ "px")
        , style "background-color" "white"
        , style "color" "black"
        , style "border" "2px solid #555555"
        , style "opacity" "0.5"
        -- , style "padding" "2%"
        ] ++ extra )
        [ Html.Styled.text
            content
        ]
