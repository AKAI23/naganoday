module UI.Chara exposing (..)

import Html.Styled exposing (Html, img)
import Html.Styled.Attributes exposing (src, style)


type Light
    = Normal
    | Dark


type alias Image =
    { src : String
    , light : Light
    }


defaultImg : Image
defaultImg =
    Image "default_pict" Normal


view_chara_right : Image -> Html msg
view_chara_right chara =
    let
        light =
            case chara.light of
                Normal ->
                    "(1)"

                Dark ->
                    "(0.5)"
    in
    img
        [ src chara.src
        , style "top" "0%"
        , style "left" "52%"
        , style "height" "100%"
        , style "width" "38%"
        , style "outline" "none"
        , style "position" "absolute"
        , style "filter" ("brightness" ++ light)
        ]
        []


view_chara_left : Image -> Html msg
view_chara_left chara =
    let
        light =
            case chara.light of
                Normal ->
                    "(1)"

                Dark ->
                    "(0.5)"
    in
    img
        [ src chara.src
        , style "top" "0%"
        , style "left" "10%"
        , style "height" "100%"
        , style "width" "38%"
        , style "outline" "none"
        , style "position" "absolute"
        , style "filter" ("brightness" ++ light)
        ]
        []


view_chara_mid : Image -> Html msg
view_chara_mid chara =
    img
        [ src chara.src
        , style "top" "0%"
        , style "left" "31%"
        , style "height" "100%"
        , style "width" "38%"
        , style "outline" "none"
        , style "position" "absolute"
        ]
        []
