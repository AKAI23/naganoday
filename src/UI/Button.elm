module UI.Button exposing (..)

import Debug exposing (toString)
import Html.Styled exposing (..)
import Html.Styled.Attributes exposing (style)
import Html.Styled.Events exposing (onClick)
import Message exposing (Msg(..))


type alias Button =
    { lef : Float
    , to : Float
    , wid : Float
    , hei : Float
    , content : String
    , effect : Msg
    , display : String
    }


test_button : Button -> Html Msg
test_button but =
    button
        [ style "top" (toString but.to ++ "%")
        , style "left" (toString but.lef ++ "%")
        , style "height" (toString but.hei ++ "%")
        , style "width" (toString but.wid ++ "%")
        , style "cursor" "pointer"
        , style "outline" "none"
        , style "padding" "0"
        , style "position" "absolute"
        , onClick but.effect
        , style "font-size" "20px"
        , style "background-color" "white"
        , style "color" "black"
        , style "border" "2px solid #555555"
        , style "opacity" "0.5"
        ]
        [ text but.content ]


trans_button : Button -> Html Msg
trans_button but =
    button
        [ style "top" (toString but.to ++ "%")
        , style "left" (toString but.lef ++ "%")
        , style "height" (toString but.hei ++ "%")
        , style "width" (toString but.wid ++ "%")
        , style "border" "0"
        , style "outline" "none"
        , style "padding" "0"
        , style "position" "absolute"
        , style "background-color" "Transparent"
        , onClick but.effect
        , style "font-size" "10px"
        ]
        []
