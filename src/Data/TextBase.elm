module Data.TextBase exposing (..)

import Array exposing (fromList, get)
import Maybe exposing (withDefault)
import Pages.Game.ChoosePage
import Pages.Game.DialogPage
import Pages.Game.SelfPage
import Pages.Game.SpacePage
import Pages.Page exposing (Page(..))
import Public.Script exposing (allOriginalText)
import String exposing (toInt, words)


type alias TextBase =
    { currentBase : Int
    , nextBase : Int
    , leftPages : List Page
    }


load_new_base : Int -> TextBase
load_new_base id =
    --add some calculations
    let
        --https://package.elm-lang.org/packages/BrianHicks/elm-csv/latest/Csv-Decode
        originalString =
            getOriginalString id allOriginalText

        --allPOriginalText里的每个string是一个base
        ( pages, nextID ) =
            csvToPage originalString
    in
    TextBase id nextID pages



-- Debug.todo ("Now Base is " ++ (Debug.toString originalString))


csvToPage : String -> ( List Page, Int )
csvToPage all =
    let
        listString =
            words all

        listIndex =
            List.range 0 (List.length listString - 2)

        headTwoNums =
            List.head listString |> withDefault "0,0" |> String.split "," |> List.take 2

        --need
        nextIndex =
            List.reverse headTwoNums |> List.head |> withDefault "0" |> toInt |> withDefault 0

        -- need to be auto
    in
    ( List.map2 convert (List.drop 1 listString) listIndex, nextIndex )



--map2


convert : String -> Int -> Page
convert line index =
    let
        list_content =
            String.split "," line

        ( subPage, background ) =
            case List.head list_content of
                Just "3" ->
                    Pages.Game.SpacePage.construct list_content

                Just "1" ->
                    Pages.Game.SelfPage.construct list_content

                Just "2" ->
                    Pages.Game.DialogPage.construct list_content

                Just "4" ->
                    Pages.Game.ChoosePage.construct list_content

                _ ->
                    Debug.todo "No such page"
    in
    GamePage subPage background index


getOriginalString : Int -> List String -> String
getOriginalString index list =
    let
        arr =
            fromList list

        maybeString =
            get index arr

        res =
            case maybeString of
                Just oribase ->
                    oribase

                _ ->
                    Debug.todo (Debug.toString index)
    in
    res
